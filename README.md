Free source code for [Learn Kubernetes book](https://leanpub.com/kubernetes) demos and do-it-yourself

# Get the book

Buy the book here:

* print book on [Lulu](http://www.lulu.com/content/livre-%c3%a0-couverture-souple/learn-kubernetes-docker---net-core-java-nodejs-php-or-python/26217672).
* ebook on [Amazon](https://mybook.to/kubernetes), [Leanpub](https://leanpub.com/kubernetes).

# Get the source code

Just `git clone` that repository. If that sounds obscure to you, click the "Downloads" link at the left of this window.

# Book TOC

1. Why Kubernetes
2. Kubernetes cluster
3. Tooling
4. Running pods
5. Exposing services
6. Volumes
7. Configuration
8. Updating and scaling
9. Sharing a cluster
10. Helm
